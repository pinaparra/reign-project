# Reign Project

Proyecto hecho a modo de prueba para Reign
El objetivo de este desarrollo es facilitar al usuario los  artículos que se han subido a blogs y redireccionar a ellos.

#Requisitos
-Se necesita tener una instalación de Docker.
-Tener nodejs instalado.

# Instrucciones de arranque

-Clonar proyecto a algún directorio que usted desee.
-Ejecutar comando docker-compose up para levantar el proyecto.

#  Configurar base de datos: 
	entrar a carpeta setup dentro del repositorio y ejecutar los siguientes comandos:
			npm install mongo
			npm install mongodb
			npm install fs
	al terminar, ejecutar esta serie de comandos: 
		    node crearBD.js 
			node crearColeccion.js
			node insertarBase.js

#Ingresar a la página
	Terminado el proceso de creación y llenado de base de datos, ingresamos a la url : http://localhost:4200 .
	ahí estará la página lista para pruebas.

