var express = require('express');
var MongoClient = require("mongodb").MongoClient, reign = require("assert");
var cron = require('node-cron');
var app = express();
var https = require('https');

MongoClient.connect("mongodb://0.0.0.0:27017", function(err,db) {
	if(err) return console.log(err);
	database = db.db('reign');
	database.collection('reign').find().toArray( (error, datos) =>{
		console.log("Entramos en reign");
	})
	
});

cron.schedule('0 0-23/2 * * *', () => {	
	var str = "";
	https.get("https://hn.algolia.com/api/v1/search_by_date?query=nodejs", (response) => {
		response.on('data', function (d) {
			str += d.toString();
        });
        response.on('end', function () {
			var request = JSON.parse(str);
			buscaNbHits(request);         
        });
	});
});


var port = process.env.PORT || 7117;
app.listen(port,function() {
	console.log('Servidor escuchando en puerto ' + port);
});

app.get('/consultar', function(req,res){
	    res.header("Access-Control-Allow-Origin", "*");
		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	var listaResultados = [];
	database.collection('reign').find().toArray( (error, datos) => {
		if(error){
			console.log(error);
			res.send("Error al consultar");
		}	
		datos.forEach(function(registro){
			listaResultados.push(registro.hits);
		});
		res.send(listaResultados);
	});
});

app.get('/eliminar/:storyid', function(req,res){
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	database.collection('reign').find().toArray( (error, datos) =>{
		encuentraRegistro(req.params.storyid,datos);
		res.send({"status":"OK"});
	});

});

function encuentraRegistro(id,datos){
	datos.forEach(function(registro){
		encuentraHits(id,registro);
	});
};

function encuentraHits(id,registro){
	registro.hits.forEach(function(hit) {
		if(hit.objectID == id){
			registro.hits.splice(hit,1);
			eliminarRegistro(registro);	
		}
	});
};

function eliminarRegistro(registro){
	var query = { "_id" : registro._id }
	database.collection('reign').deleteOne(query, function(err,ok){
		if(err) throw err;
		insertarRegistro(registro);	
	});
}

function insertarRegistro(registro){
	var registroCopiado = registro;
		database.collection('reign').insertOne(registroCopiado, function(err, res){
			if(err) throw err;
		});	
}

function buscaNbHits(nuevaEntrada){
		var encontrado = false;
	 	database.collection('reign').find().toArray( (error, datos) =>{
		    datos.forEach(function(registro){
				if(registro.nbHits == nuevaEntrada.nbHits){
					encontrado = true;
				}	
			});
			if(!encontrado){
				insertarRegistro(nuevaEntrada);
			}
			
		});
}