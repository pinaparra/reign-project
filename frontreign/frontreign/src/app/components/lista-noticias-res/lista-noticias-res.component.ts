import { Component, OnInit } from '@angular/core';
import { DataServiceRestService } from '../../services/data-service-rest.service';

const oneday = 60 * 60 * 24 * 1000;
@Component({
  selector: 'lista-noticias-res',
  templateUrl: './lista-noticias-res.component.html',
  styleUrls: ['./lista-noticias-res.component.css']
})
export class ListaNoticiasResComponent implements OnInit {
  
  hits: any[] = [];

  constructor(public dataServiceRest:DataServiceRestService) { }

  ngOnInit(): void {
    this.consultarHits();
  }

  calcularFecha(date:any){
    let now = +new Date();
    var compare = (now - date.created_at) > oneday;
    if(compare){
      return "yesterday";
    }else{
      return date.created_at;
    }
  }

  consultarHits(){
    this.dataServiceRest.getHits()
    .subscribe(
      (data) => {
        data[0].forEach(element => {
          if(element.story_title || element.title){
            this.hits.push(element);
          }
        });
       
      },
      (error) => {
        console.log(error);
      }
    )
  }

  eliminarRegistro(id:string){
   this.dataServiceRest.deleteHit(id)
   .subscribe(
     (data) => {
       if(data){
         this.consultarHits();
       }
     },
     (error) => {
       console.log(error);
     }
   );
  }

}
