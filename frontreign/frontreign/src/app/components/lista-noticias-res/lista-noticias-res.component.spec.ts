import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaNoticiasResComponent } from './lista-noticias-res.component';

describe('ListaNoticiasResComponent', () => {
  let component: ListaNoticiasResComponent;
  let fixture: ComponentFixture<ListaNoticiasResComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaNoticiasResComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaNoticiasResComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
