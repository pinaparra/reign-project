import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderHrComponent } from './components/header-hr/header-hr.component';
import { ListaNoticiasResComponent } from './components/lista-noticias-res/lista-noticias-res.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { DataServiceRestService } from './services/data-service-rest.service'; 
import { MatListModule } from '@angular/material/list';

@NgModule({
  declarations: [ 
    AppComponent,
    HeaderHrComponent,
    ListaNoticiasResComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatListModule
  ],
  providers: [DataServiceRestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
