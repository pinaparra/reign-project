import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
const PATH = 'http://localhost:7117';
@Injectable({
  providedIn: 'root'
})

export class DataServiceRestService {
  constructor( private http: HttpClient){
    
  }

  getHits() {
      return this.http.get(PATH + "/consultar");  
  }

  deleteHit(id:string) {
      return this.http.get(PATH + "/eliminar/"+id);
  }
 
}







