import { TestBed } from '@angular/core/testing';

import { DataServiceRestService } from './data-service-rest.service';

describe('DataServiceRestService', () => {
  let service: DataServiceRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataServiceRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
