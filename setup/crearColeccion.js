var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://0.0.0.0:27017/";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("reign");
  dbo.createCollection("reign", function(err, res) {
    if (err) throw err;
    console.log("Colleccion creada correctamente");
    db.close();
  });
});