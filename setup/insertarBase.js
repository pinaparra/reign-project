var MongoClient = require('mongodb').MongoClient;
var fs = require('fs');
var url = "mongodb://0.0.0.0:27017/";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("reign");
  
  fs.readFile("./Data.json", "utf-8",function(err, data) {
	  if (err) {
		  return console.log(err);
	  }
	dbo.collection("reign").insertOne(JSON.parse(data), function(err, res) {
		if (err) throw err;
		console.log("Registro insertado correctamente");
		db.close();
  });
  });
});